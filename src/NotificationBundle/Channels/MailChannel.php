<?php

namespace NotificationBundle\Channels;

use Swift_Mailer;
use Swift_Message;
use Swift_Transport_EsmtpTransport;
use NotificationBundle\Contracts\ChannelInterface;
use NotificationBundle\Contracts\NotificationInterface;

class MailChannel implements ChannelInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Swift_Transport_EsmtpTransport
     */
    private $transport;

    /**
     * Create a new mail channel instance.
     *
     * @param Swift_Mailer $mailer
     * @param Swift_Transport_EsmtpTransport $transport
     */
    public function __construct(Swift_Mailer $mailer, Swift_Transport_EsmtpTransport $transport)
    {
        $this->mailer = $mailer;
        $this->transport = $transport;
    }

    /**
     * @inheritdoc
     */
    public function send($notifiable, NotificationInterface $notification)
    {
        $data = $this->getData($notifiable, $notification);

        $message = (new Swift_Message($data['title']))
            ->setFrom($data['from']['senderEmail'], $data['from']['displayName'])
            ->setTo($data['to'])
            ->setBody(
                $data['body'],
                'text/html'
            );

        # $message->getHeaders()->addTextHeader('X-Mail-Category', '');
        # $message->getHeaders()->addTextHeader('X-Site-Country', 'eg');

        $sent = $this->mailer->send($message);

        $spool = $this->mailer->getTransport()->getSpool();
        $spool->flushQueue($this->transport);

        return $sent;
    }

    /**
     * @inheritdoc
     */
    public function getData($notifiable, NotificationInterface $notification)
    {
        if (method_exists($notification, 'toMail')) {
            return $notification->toMail($notifiable);
        }

        throw new \RuntimeException(sprintf(
            'Notification [%s] is missing "toMail" method.',
            (new \ReflectionClass($notification))->getShortName()
        ));
    }
}