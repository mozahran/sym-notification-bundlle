<?php

namespace NotificationBundle;

use NotificationBundle\Contracts\NotificationLoggerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotificationSubscriber implements EventSubscriberInterface
{
    /**
     * @var NotificationLogger
     */
    private $logger;

    /**
     * Create a new instance of the notification subscriber.
     *
     * @param NotificationLoggerInterface $logger
     */
    public function __construct(NotificationLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'notification.sent' => 'notificationSent',
            'notification.failed' => 'notificationFailed'
        ];
    }

    /**
     * Handles the triggered event when a notification is sent.
     *
     * @param Event $event
     */
    public function notificationSent(Event $event)
    {
        $this->logger->success($event);
    }

    /**
     * Handles the triggered event when a notification is sent.
     *
     * @param Event $event
     */
    public function notificationFailed(Event $event)
    {
        $this->logger->error($event);
    }
}