<?php

namespace NotificationBundle\Controller;

use NotificationBundle\Notifications\UserCreated;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $sender = $this->get('notification_sender');

        $user = $this->get('doctrine.orm.entity_manager')->getRepository(User::class)->find(1);

        $sender->send($user, new UserCreated('asd'));

        return $this->render('@Notification/Default/index.html.twig');
    }
}
