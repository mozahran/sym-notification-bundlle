<?php

namespace NotificationBundle\Contracts;

interface RabbitMQManagerInterface
{
    /**
     * Open a RabbitMQ Connection.
     */
    public function openConnection();

    /**
     * Send a message to the queue.
     *
     * @param $message
     */
    public function sendMessage($message);

    /**
     * Close the opened RabbitMQ channel and connection.
     */
    public function closeConnection();
}