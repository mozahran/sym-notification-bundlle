<?php

namespace NotificationBundle\Contracts;

interface ChannelInterface
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param NotificationInterface $notification
     *
     * @return bool
     */
    public function send($notifiable, NotificationInterface $notification);

    /**
     * Get the data for the notification.
     *
     * @param $notifiable
     * @param NotificationInterface $notification
     *
     * @return array
     */
    public function getData($notifiable, NotificationInterface $notification);
}