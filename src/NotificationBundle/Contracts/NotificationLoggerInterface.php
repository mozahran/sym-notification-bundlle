<?php

namespace NotificationBundle\Contracts;

use Symfony\Component\EventDispatcher\Event;

interface NotificationLoggerInterface
{
    /**
     * Log a message to the notifications log.
     *
     * @param $message
     *
     * @return mixed
     */
    public function log($message);

    /**
     * Log a successful notification to the log.
     *
     * @param Event $event
     *
     * @return mixed
     */
    public function success(Event $event);

    /**
     * Log a failed notification to the log.
     *
     * @param Event $event
     *
     * @return mixed
     */
    public function error(Event $event);
}