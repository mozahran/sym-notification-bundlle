<?php

namespace NotificationBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use NotificationBundle\Contracts\NotificationInterface;

class NotificationFailed extends Event
{
    /**
     * The notifiable entity who received the notification.
     *
     * @var mixed
     */
    public $notifiable;

    /**
     * The notification instance.
     *
     * @var NotificationInterface
     */
    public $notification;

    /**
     * The channel name.
     *
     * @var string
     */
    public $channel;

    /**
     * Create a new event instance.
     *
     * @param mixed $notifiable
     * @param NotificationInterface $notification
     * @param string $channel
     * @param mixed $response
     */
    public function __construct($notifiable, NotificationInterface $notification, string $channel, $response = null)
    {
        $this->notifiable = $notifiable;
        $this->notification = $notification;
        $this->channel = $channel;
    }
}