<?php

namespace NotificationBundle;

use NotificationBundle\Contracts\RabbitMQManagerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RabbitMQManager implements RabbitMQManagerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * RabbitMQ configurations array.
     *
     * @var array
     */
    private $config;

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var \PhpAmqpLib\Channel\AMQPChannel
     */
    private $channel;

    /**
     * RabbitMQManager constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $container->getParameter('aqarmap_notification')['rabbitmq'];
    }

    /**
     * @inheritdoc
     */
    public function openConnection()
    {
        $this->connection = new AMQPStreamConnection(
            $this->config['host'],
            $this->config['port'],
            $this->config['user'],
            $this->config['password'],
            $this->config['vhost'],
            false,      // insist
            'AMQPLAIN', // login_method
            null,       // login_response
            'en_US',    // locale
            3,
            3,
            null,
            false,
            0
        );

        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->config['queue'], false, false, false, false);
    }

    /**
     * @inheritdoc
     */
    public function sendMessage($message)
    {
        $msg = new AMQPMessage($message);

        $this->channel->basic_publish($msg, '', $this->config['queue']);
    }

    /**
     * @inheritdoc
     */
    public function closeConnection()
    {
        $this->channel->close();
        $this->connection->close();
    }
}