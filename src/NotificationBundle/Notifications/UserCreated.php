<?php

namespace NotificationBundle\Notifications;

use UserBundle\Entity\User;
use NotificationBundle\Contracts\ShouldQueue;
use NotificationBundle\Contracts\NotificationInterface;

class UserCreated implements NotificationInterface, ShouldQueue
{
    /**
     * The notified entity.
     *
     * @var User
     */
    private $user;

    /**
     * UserCreated constructor.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification channel.
     *
     * @param mixed $notifiable
     *
     * @return string|array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'notification_id' => $this->user->getId(),
            'notification_type' => __CLASS__,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toMail($notifiable)
    {
        return [
            'title' => 'asdasdas',
            'from' => [
                'displayName' => 'Mohamed Zahran',
                'senderEmail' => 'm.zahran90@gmail.com'
            ],
            'to' => 'test@example.com',
            'body' => 'lorem ipsum',
        ];
    }
}